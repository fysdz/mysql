<?php

include "vendor/autoload.php";

use muyomu\config\ConfigApi;
use muyomu\database\mysql\config\DataSourceConfig;
use muyomu\database\mysql\connector\PdoMysql;
use muyomu\database\mysql\Data;
use muyomu\database\mysql\Result\Mode;

ConfigApi::configure(DataSourceConfig::class,array(
    "dataSource"=>"muyomu",
    "pool"=>[
        "muyomu"=>[
            "hostname"=>"192.168.10.128",
            "port"=>3306,
            "database"=>"muyomu",
            "user"=>[
                "username"=>"root",
                "password"=>"123456"
            ],
            "parameters"=>[

            ]
        ]
    ]
));

$kk = new PdoMysql(Data::class);

$kk->query("getUser",array("name"=>"liuzhang"));

$kkd =$kk->getResult(Mode::RESULT_SET);

echo json_encode($kkd,JSON_UNESCAPED_UNICODE);

