<?php

namespace muyomu\database\mysql\client;

use muyomu\database\mysql\Result\Mode;

interface ResultClient
{
    public function getResult(Mode $mode): bool | int | array;
}