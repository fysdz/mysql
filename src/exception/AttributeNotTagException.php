<?php

namespace muyomu\database\mysql\exception;

use Exception;

class AttributeNotTagException extends Exception
{
    public function __construct()
    {
        parent::__construct("QueryAttributeNotTagException");
    }
}