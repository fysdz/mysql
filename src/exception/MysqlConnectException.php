<?php

namespace muyomu\database\mysql\exception;

use Exception;

class MysqlConnectException extends Exception
{
    public function __construct()
    {
        parent::__construct("Data error");
    }
}