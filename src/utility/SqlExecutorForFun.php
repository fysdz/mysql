<?php

namespace muyomu\database\mysql\utility;

use muyomu\database\mysql\client\SqlQueryForFun;
use mysqli;
use mysqli_result;
use ReflectionException;
use ReflectionMethod;
use muyomu\database\mysql\exception\AttributeNotTagException;

class SqlExecutorForFun implements SqlQueryForFun
{
    private SqlUtility $sqlUtility;

    public function __construct(){

        $this->sqlUtility = new SqlUtility();
    }

    /**
     * @throws AttributeNotTagException
     * @throws ReflectionException
     */
    public function sqlExecutor(mysqli $con, object $class, ReflectionMethod $method, array $args): bool | mysqli_result
    {
        return $con->query($this->sqlUtility->getSql($class, $method, $args));
    }
}