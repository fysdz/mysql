<?php

namespace muyomu\database\mysql;

use muyomu\database\mysql\annotation\Query;
use muyomu\database\mysql\annotation\Repository;

#[Repository("muyomu")]
class Data
{
    #[Query("select * from account where name = ':name'")]
    public function getUser(string $name): array
    {
        return array("name" => $name);
    }
}